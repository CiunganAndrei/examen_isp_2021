package Examen;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Exercitiul2 extends JFrame {

    JTextField t1,t2;
    JButton Button;

    Exercitiul2(){

        setTitle("Ex2");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(200,300);
        setVisible(true);
    }
    public void init() {

        this.setLayout(null);
        int width = 80;
        int height = 20;

        t1 = new JTextField();
        t1.setBounds(50, 50, width, height);

        t2 = new JTextField();
        t2.setBounds(50, 100, width, height);


        Button = new JButton("Buton");
        Button.setBounds(50, 150, width, height);
        Button.addActionListener(new Exercitiul2.ActiuneButon());

    add(t1);add(t2);add(Button);
    }

    public class ActiuneButon implements ActionListener {
        public void actionPerformed(ActionEvent e) {
                String filename = t1.getText();
                int apparitions = 0;

            BufferedReader in = null;
            try {
                in = new BufferedReader(
                        new FileReader("C:\\Users\\Andi\\Desktop\\Examen_isp_2021\\examen_isp_2021\\src\\main\\java\\Examen\\"+filename+".txt"));
            } catch (FileNotFoundException fileNotFoundException) {
                fileNotFoundException.printStackTrace();
            }

            String string = new String();

                while (true) {
                    try {
                        if (!((string = in.readLine()) != null)) break;
                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    }
                    char c[] = string.toCharArray();
                    for (int i = 0; i < c.length; i++) {
                       apparitions++;
                    }
                }
                t2.setText(String.valueOf(apparitions));
            }
        }

    public static void main(String[] args) {
        new Exercitiul2();
    }

    }